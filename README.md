# 概要

- Twitter APIへのOAuth認証を行うライブラリ
- OAuthのバージョン1.0に対応

# ビルドに必要な環境

* [Gradle](https://gradle.org/) >= 2.9
* TwitterアカウントとAPIへのアクセスキー
    * [Twitter](https://twitter.com/?lang=ja)
    * [Application Management](https://apps.twitter.com/)

        * ※ アクセスキーの取得にはTwitterアカウントでの電話番号による認証が必要
        * ※ アクセスキーの発行はApplication Managementから行う

## ビルドとテスト


1. 好きなディレクトリに`git clone https://sevenspice@bitbucket.org/sevenspice/oauth_twitter.git`でプロジェクトを取得
2. ターミナルからプロジェクトに移動
3. `git submodule init`でサブモジュールを初期化
4. `git submodule update`でサブモジュールを取得
5. `cd urlconnect_wrapper`でサブモジュールディレクトリに移動
6. `gradle build`でサブモジュールをビルド
7. プロジェクトのルートディレクトリに戻る(`cd ../`)
8. `cd ./master`で`master`ディレクトリに移動
9. `cp ./src/test/resources/token.properties.origin ./src/test/resources/token.properties`でコピー
10. `vi ./src/test/resources/token.properties`でファイルにAPIで使用するキーを追記して保存
11. `gradle build`でビルド
    * テストを行わなずビルドする場合は`gradle build -x test`
12. `build/libs/`ディレクトリ直下に`oauth_twitter.jar`が生成されていることを確認

// パッケージ宣言
package biz.libran.twitter;

import biz.libran.http.Connection;
import biz.libran.http.IHttpConnectionCallback;
import biz.libran.http.Utility;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.HashMap;
import java.util.Random;
import java.util.Base64;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Twitter APIのOAuth認証を実現するクラス
 */
public class OAuth extends Connection{
    private static final Logger logger = Logger.getLogger(OAuth.class.getName());

    // クラス内定数
    private static final String CODE_TABLE = "0123456789abcdef";
    private static final String REQUEST_HEADER_KEYNAME_CONTENTTYPE   = "Content-Type";
    private static final String REQUEST_HEADER_KEYNAME_AUTHORIZATION = "Authorization";
    private static final String REQUEST_HEADER_VALUE_CONTENTTYPE = "application/x-www-form-urlencoded";

    // メンバー
    private final String consumerKey;
    private final String consumerSecret;
    private String accessToken;
    private String accessTokenSecret;

    public String getConsumerKey(){
        return this.consumerKey;
    }

    public String getConsumerSecret(){
        return this.consumerSecret;
    }

    public String getAccessToken(){
        return this.accessToken;
    }

    public void setAccessToken(String accessToken){
        this.accessToken = accessToken;
    }

    public String getAccessTokenSecret(){
        return this.accessTokenSecret;
    }

    public void setAccessTokenSecret(String accessTokenSecret){
        this.accessTokenSecret = accessTokenSecret;
    }

    /**
     * コンストラクタ
     * @param protocol GETまたはPOSTを指定可能
     * @param scheme   スキームを指定
     * @param host     ホストを指定
     * @param path     パスを指定
     * @param consumerKey       コンシューマーキーを指定
     * @param consumerSecret    コンシューマーシークレットキーを指定
     * @param accessToken       アクセストークンを指定
     * @param accessTokenSecret アクセストークンシークレットを指定
     */
    public OAuth(
        String protocol,
        String scheme,
        String host,
        String path,
        String consumerKey,
        String consumerSecret,
        String accessToken,
        String accessTokenSecret
    ){

        super(protocol, scheme, host, path);
        this.consumerKey    = consumerKey;
        this.consumerSecret = consumerSecret;
        this.accessToken       = accessToken;
        this.accessTokenSecret = accessTokenSecret;

    }

    /**
     * OAuth通信を実行する
     * @param apiQueries 通信時に渡すクエリパラメーターを指定
     * @return レスポンス内容をリストで返却
     */
    public List<String> oauth(Map<String, String> apiQueries){
        return this.oauth(apiQueries, null);
    }

    /**
     * OAuth通信を実行する
     * @param apiQueries 通信時に渡すクエリパラメーターを指定
     * @param httpConnectionCallback 通信結果を処理するラムダを指定
     * @return レスポンス内容をリストで返却
     */
    public List<String> oauth(
            Map<String, String> apiQueries,
            IHttpConnectionCallback httpConnectionCallback
    ){

        List<String> content = new ArrayList<String>();
        String nonce     = this.createNonce();
        String timestamp = String.valueOf((System.currentTimeMillis() / 1000L));
        String key       = this.getConsumerSecret() + '&' + this.getAccessTokenSecret();

        // インスタンスにクエリを設定
        this.setQueries(apiQueries);

        // 署名用文字列の作成
        String signatureString = this.createSignatureString(this.generateAuthorizationParameters(nonce, timestamp));
        // 署名
        String signature = this.signBase64(signatureString, key);
        // 認証用文字列の作成
        String authorizationString = this.createAuthorizationString(
                this.generateAuthorizationParameters(nonce, timestamp, signature));

        // インスタンスにリクエストヘッダをセット
        Map<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put(REQUEST_HEADER_KEYNAME_CONTENTTYPE, REQUEST_HEADER_VALUE_CONTENTTYPE);
        requestHeaders.put(REQUEST_HEADER_KEYNAME_AUTHORIZATION, authorizationString);
        this.setHeaders(requestHeaders);

        // 通信を実行
        if(httpConnectionCallback != null){
            this.execute(httpConnectionCallback);
        }else{
            content = this.execute();
        }

        return content;

    }

    /**
     * リクエストヘッダーに追加する認証文字列を作成
     * @param parameters 認証に必要なパラメーターを指定
     * @return パラメーターを結合した文字列を返却
     */
    private String createAuthorizationString(Map<String, String> parameters){

        String authorizationString = "OAuth ";


        for(Map.Entry<String, String> map : parameters.entrySet()){

            String query = map.getKey() + "=\"" + this.urlEncode(map.getValue()) + "\", ";
            authorizationString += query;

        }

        return authorizationString.substring(0, (authorizationString.length() - 2));

    }

    /**
     * 指定された文字列からハッシュを生成する
     * @param target ハッシュを生成する文字列を指定
     * @param key    ハッシュ生成に使用する鍵を指定
     * @return 生成されたハッシュを返却
     */
    private String signBase64(String target,String key){

        String hash = "";

        try{

            SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(secretKeySpec);

            // ハッシュ生成
            byte[] raw = mac.doFinal(target.getBytes());

            // BASE64でエンコード
            hash = Base64.getEncoder().encodeToString(raw);


        }catch(NoSuchAlgorithmException e){
            logger.log(Level.SEVERE, "method failed generate a hash", e);
        }catch(InvalidKeyException e){
            logger.log(Level.SEVERE, "method failed generate a hash", e);
        }

        return hash;

    }

    /**
     * 署名用文字列を作成する
     * @param parameters 認証に必要なパラメーターを指定
     * @return パラメーターを結合した文字列を返却
     */
    private String createSignatureString(Map<String, String> parameters){


        String url = this.getScheme() + "://" +  this.getHost() + this.getPath();
        String signatureString = this.getProtocol() + '&' + Utility.urlEncode(url) + '&';

        for(Map.Entry<String, String> map : parameters.entrySet()){

            String query = map.getKey() + '=' + map.getValue() + '&';
            signatureString += this.urlEncode(query);

        }

        // 末尾のエンコードされた&を削除して返却
        return signatureString.substring(0, (signatureString.length() - 3));

    }

    /**
     * APIへのアクセスに必要な認証パラメーターを生成する
     * @param nonce     リクエストを識別する一意の値を指定
     * @param timestamp リクエストの送信時間を指定
     * @return 認証パラメーターを返却
     */
    private Map<String, String>  generateAuthorizationParameters(String nonce, String timestamp){
       return  this.generateAuthorizationParameters(nonce, timestamp, "");
    }


    /**
     * APIへのアクセスに必要な認証パラメーターを生成する
     * @param nonce     リクエストを識別する一意の値を指定
     * @param timestamp リクエストの送信時間を指定
     * @param verifier  APIへの認証文字列を指定
     * @param signature 署名を指定
     * @return 認証パラメーターを返却
     */
    private Map<String, String> generateAuthorizationParameters(String nonce, String timestamp, String signature){

        Map<String, String> parameters = new TreeMap<String, String>();

        // OAuth のパラメーターで初期化
        parameters.put("oauth_token",        this.getAccessToken());
        parameters.put("oauth_consumer_key", this.getConsumerKey());
        parameters.put("oauth_nonce",        nonce);
        parameters.put("oauth_timestamp",    timestamp);
        parameters.put("oauth_version",          "1.0");
        parameters.put("oauth_signature_method", "HMAC-SHA1");

        // クエリがセットされている場合
        if((this.getQueries() != null) && (this.getQueries().size() > 0)){

            for(Map.Entry<String, String> map : this.getQueries().entrySet()){
                parameters.put(map.getKey(), this.urlEncode(map.getValue()));
            }

        }

        // 署名を引数に指定された場合
        if(!signature.isEmpty()){
            parameters.put("oauth_signature", signature);
        }

        return parameters;

    }

    /**
     * APIへのリクエストを識別する一意の値を生成する
     */
    private String createNonce(){

        String nonce = "";
        int digit = (CODE_TABLE.length() * 2);

        Random random = new Random();
        for(int i = 0;i<digit;i++){
            int index = random.nextInt(CODE_TABLE.length());
            nonce += CODE_TABLE.charAt(index);
        }

        return nonce;

    }

    /**
     * OAuth認証のためのURLエンコードを行う
     * @param target エンコードする文字列を指定
     * @return エンコードされた文字列を返却
     */
    private String urlEncode(String target){

        String encoded = Utility.urlEncode(target);
        return encoded.replace("%2d", "-");

    }



}

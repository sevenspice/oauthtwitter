// パッケージ宣言
package biz.libran.twitter;

import biz.libran.http.Connection;
import org.junit.Test;
import org.junit.Before;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import org.apache.commons.lang3.RandomStringUtils;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * OAuth クラスの単体テスト
 */
public class OAuthTest {

    private String protocol;
    private String scheme;
    private String host;
    private String path;
    private String consumerKey;
    private String consumerSecret;
    private String accessToken;
    private String accessTokenSecret;

    @Before
    public void initialize(){

        this.protocol = "POST";
        this.scheme   = "https";
        this.host     = "api.twitter.com";
        this.path     = "/1.1/statuses/update.json";

        // リソースファイルから初期化
        ResourceBundle bundle = ResourceBundle.getBundle("token");
        this.consumerKey    = bundle.getString("CONSUMER_KEY");
        this.consumerSecret = bundle.getString("CONSUMER_SECRET");
        this.accessToken       = bundle.getString("ACCESS_TOKEN");
        this.accessTokenSecret = bundle.getString("ACCESS_TOKEN_SECRET");

    }

    @Test
    /**
     * インスタンスの生成をテスト
     */
    public void generateOAuth(){

        OAuth oauth = this.generateInstance();

        assertThat(oauth.getConsumerKey(),    is(this.consumerKey));
        assertThat(oauth.getConsumerSecret(), is(this.consumerSecret));
        assertThat(oauth.getAccessToken(),       is(this.accessToken));
        assertThat(oauth.getAccessTokenSecret(), is(this.accessTokenSecret));

    }

    @Test
    public void oauthNoArgument(){

       String comment = "テスト投稿 " + RandomStringUtils.randomAscii(10);
       OAuth oauth = this.generateInstance();

       // クエリ
       Map<String, String> queries = new HashMap<String, String>();
       queries.put("status", comment);

       // POSTでもクエリをURLに追加する
       oauth.setQueriesAdd(true);

       // コールバックなしを実行
       List<String> content = oauth.oauth(queries);
       assertThat("200", is(content.get(0)));

        try{
           Thread.sleep(3000); //3000ミリ秒Sleepする
        }catch(InterruptedException e){}

    }

    @Test
    public void oauth(){

       String comment = "テスト投稿 " + RandomStringUtils.randomAscii(10);
       OAuth oauth = this.generateInstance();

       // クエリ
       Map<String, String> queries = new HashMap<String, String>();
       queries.put("status", comment);

       // POSTでもクエリをURLに追加する
       oauth.setQueriesAdd(true);

       // コールバックなしを実行
       List<String> content = oauth.oauth(queries, null);
       assertThat("200", is(content.get(0)));

       comment = "テスト投稿 " + RandomStringUtils.randomAscii(10);
       queries.put("status", comment);

       try{
           Thread.sleep(3000); //3000ミリ秒Sleepする
        }catch(InterruptedException e){}

       // コールバックありを実行
       try{

           content = oauth.oauth( queries, (Connection c) -> {

               try{

                   int code = c.getConnection().getResponseCode();
                   assertThat(200, is(code));

               }catch(Exception e){
                   assertThat(true, is(false));
               }

           });

       }catch(Exception e){
           assertThat(true, is(false));
       }

       // コールバック処理の場合は戻り値のリストは空であること
       assertThat(0, is(content.size()));

    }

    @Test
    public void createAuthorizationString()
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException{

            String comparison = "OAuth oauth_consumer_key=\"xvz1evFS4wEEPTGEFPHBog\", oauth_nonce=\"kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg\", oauth_signature_method=\"HMAC-SHA1\", oauth_timestamp=\"1318622958\", oauth_token=\"370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb\", oauth_version=\"1.0\", status=\"%25E8%2589%25A6%25E3%2581%2593%25E3%2582%258C\"";

            String consumerKey = "xvz1evFS4wEEPTGEFPHBog";
            String accessToken = "370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb";
            String nonce       = "kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg";
            String dateTime    = "1318622958";
            Map<String, String> queries = new HashMap<String, String>();
            queries.put("status", "艦これ");

            OAuth oauth = new OAuth(

                this.protocol,
                this.scheme,
                this.host,
                this.path,
                consumerKey,
                this.consumerSecret,
                accessToken,
                this.accessTokenSecret

            );

            // クエリのセット
            oauth.setQueries(queries);

            Method createAuthorizationString = OAuth.class.getDeclaredMethod("createAuthorizationString", Map.class);
            Method generateAuthorizationParameters = OAuth.class.getDeclaredMethod("generateAuthorizationParameters",
                    String.class, String.class, String.class );

            createAuthorizationString.setAccessible(true);
            generateAuthorizationParameters.setAccessible(true);

            // 正常系
            // 認証用の文字列が生成されること
            String result = (String) createAuthorizationString.invoke(
                                         oauth,
                                         generateAuthorizationParameters.invoke(oauth, nonce, dateTime, "" )
                                     );

            assertThat(comparison, is(result));

    }


    @Test
    public void signBase64()
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException{

            String target = "Hello World";
            String key    = "secret";

            OAuth oauth = generateInstance();

            Method signBase64 = OAuth.class.getDeclaredMethod("signBase64", String.class, String.class);
            signBase64.setAccessible(true);

            // 正常系
            // HMAC-SHA1でハッシュが生成されBASE64でエンコードされた結果が返却されること
            String hash = (String) signBase64.invoke(oauth, target, key);

            assertThat("hY2og3uH8EsFLA9ulUw/e74IEWQ=", is(hash));

    }



    @Test
    public void createSignatureString()
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException{

            String comparison = "POST&https%3A%2F%2Fapi.twitter.com%2F1.1%2Fstatuses%2Fupdate.json&oauth_consumer_key%3Dxvz1evFS4wEEPTGEFPHBog%26oauth_nonce%3DkYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1318622958%26oauth_token%3D370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb%26oauth_version%3D1.0%26status%3D%25E8%2589%25A6%25E3%2581%2593%25E3%2582%258C";

            String consumerKey = "xvz1evFS4wEEPTGEFPHBog";
            String accessToken = "370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb";
            String nonce       = "kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg";
            String dateTime    = "1318622958";
            Map<String, String> queries = new HashMap<String, String>();
            queries.put("status", "艦これ");

            OAuth oauth = new OAuth(

                this.protocol,
                this.scheme,
                this.host,
                this.path,
                consumerKey,
                this.consumerSecret,
                accessToken,
                this.accessTokenSecret

            );

            // クエリのセット
            oauth.setQueries(queries);

            Method createSignatureString = OAuth.class.getDeclaredMethod("createSignatureString", Map.class);
            Method generateAuthorizationParameters = OAuth.class.getDeclaredMethod("generateAuthorizationParameters",
                    String.class, String.class, String.class );

            createSignatureString.setAccessible(true);
            generateAuthorizationParameters.setAccessible(true);

            // 正常系
            // 署名用の文字列が生成されること
            String result = (String) createSignatureString.invoke(
                                         oauth,
                                         generateAuthorizationParameters.invoke(oauth, nonce, dateTime, "" )
                                     );

            assertThat(comparison, is(result));

    }

    @Test
    public void generateAuthorizationParameters()
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException{

            String nonce     = "0123456789abcdef";
            String dateTime  = String.valueOf((System.currentTimeMillis() / 1000L));
            String signature = "abcdefghjklmnopq";
            String version   = "1.0";
            String signatureMethod = "HMAC-SHA1";
            String includeEntities = "true";

            String searchWord = "twitter";
            String location   = "ja";

            Map<String, String> queries = new HashMap<String, String>();
            queries.put("q",        searchWord);
            queries.put("location", location);


            OAuth oauth = generateInstance();
            oauth.setQueries(queries);

            Method generateAuthorizationParameters = OAuth.class.getDeclaredMethod(

                    "generateAuthorizationParameters",
                    String.class,
                    String.class,
                    String.class

            );

            generateAuthorizationParameters.setAccessible(true);

            // 正常系
            // クエリと引数にすべて値を指定
            Map<String, String> parameters =  this.autoCast(
                generateAuthorizationParameters.invoke(oauth, nonce, dateTime, signature));

            // 必要なキーを持っているか確認する
            assertThat(parameters, hasKey("oauth_consumer_key"));
            assertThat(parameters, hasKey("oauth_nonce"));
            assertThat(parameters, hasKey("oauth_signature"));
            assertThat(parameters, hasKey("oauth_signature_method"));
            assertThat(parameters, hasKey("oauth_timestamp"));
            assertThat(parameters, hasKey("oauth_token"));
            assertThat(parameters, hasKey("oauth_version"));
            assertThat(parameters, hasKey("q"));
            assertThat(parameters, hasKey("location"));
            // 格納されている値が正しいか確認する
            assertThat(nonce,      is(parameters.get("oauth_nonce")));
            assertThat(signature,  is(parameters.get("oauth_signature")));
            assertThat(dateTime,   is(parameters.get("oauth_timestamp")));
            assertThat(searchWord, is(parameters.get("q")));
            assertThat(location,   is(parameters.get("location")));
            assertThat(version,    is(parameters.get("oauth_version")));
            assertThat(this.consumerKey, is(parameters.get("oauth_consumer_key")));
            assertThat(this.accessToken, is(parameters.get("oauth_token")));
            assertThat(signatureMethod,  is(parameters.get("oauth_signature_method")));

            // 正常系
            // クエリが空
            oauth.setQueries(new HashMap<String, String>());
            parameters =  this.autoCast(
                    generateAuthorizationParameters.invoke(oauth, nonce, dateTime, signature));
            assertThat(parameters, not(hasKey("q")));
            assertThat(parameters, not(hasKey("location")));

            // 正常系
            // クエリがNULL
            oauth.setQueries(null);
            parameters =  this.autoCast(
                    generateAuthorizationParameters.invoke(oauth, nonce, dateTime, signature));
            assertThat(parameters, not(hasKey("q")));
            assertThat(parameters, not(hasKey("location")));


            // 正常系
            // signatureが空
            parameters =  this.autoCast(
                    generateAuthorizationParameters.invoke(oauth, nonce, dateTime, ""));
            assertThat(parameters, not(hasKey("oauth_signature")));

            // オーバーロードメソッドのテスト
            generateAuthorizationParameters = OAuth.class.getDeclaredMethod(
                    "generateAuthorizationParameters",String.class,String.class);
            generateAuthorizationParameters.setAccessible(true);

            // 正常系
            // 引数にすべて値を指定
            parameters =  this.autoCast(
                generateAuthorizationParameters.invoke(oauth, nonce, dateTime ));
            assertThat(parameters, not(hasKey("oauth_signature")));

    }


    @Test
    public void createNonce()
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException{

            OAuth oauth = generateInstance();

            Method createNonce = OAuth.class.getDeclaredMethod("createNonce");
            createNonce.setAccessible(true);

            String nonce1 = (String) createNonce.invoke(oauth);
            String nonce2 = (String) createNonce.invoke(oauth);

            // 32桁であること
            assertThat(32, is(nonce1.length()));
            assertThat(32, is(nonce2.length()));

            // 一意であること
            assertThat(nonce1, is(not(nonce2)));

    }

    @Test
    public void urlEncode()
            throws NoSuchMethodException,
                   SecurityException,
                   IllegalAccessException,
                   IllegalArgumentException,
                   InvocationTargetException{

            OAuth oauth = generateInstance();

            Method urlEncode = OAuth.class.getDeclaredMethod("urlEncode", String.class);
            urlEncode.setAccessible(true);

            String encoded = (String) urlEncode.invoke(oauth, "Hello World");
            // URLエンコードされること
            assertThat("Hello%20World", is(encoded));

            encoded = (String) urlEncode.invoke(oauth, "-");
            // - はエンコードされないこと
            assertThat("-", is(encoded));

    }


    /**
     * テスト用のインスタンスを生成する
     */
    private OAuth generateInstance(){

        return new OAuth(

            this.protocol,
            this.scheme,
            this.host,
            this.path,
            this.consumerKey,
            this.consumerSecret,
            this.accessToken,
            this.accessTokenSecret

        );


    }

    @SuppressWarnings("unchecked")
    /**
     * キャストメソッド
     */
    private <T> T autoCast(Object object){

        T castObject = (T) object;
        return castObject;

    }

}
